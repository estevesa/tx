import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SrvService {

  private updateData = new BehaviorSubject<string>('1');

  protected url = 'http://localhost:8080';

  constructor(private readonly http: HttpClient) { }

  private httpOptions = {
    Headers: new HttpHeaders({
        'content-type': 'application/json'
    }),
  };

  public handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do Erro: ${error.status},` + `mensagem: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  public getImage(path: string, imgname: string): Observable<any> {
    return this.http
      .get<any>(`${this.url}/${path}/${imgname}`)
        .pipe(
          catchError(this.handleError)
        );
  }

  public getImages(path: string, fullpath: string): Observable<any> {
    const pathcompleto = `${this.url}/${path}?id=${fullpath}`;
    return this.http
      .get<any>(pathcompleto)
        .pipe(
          catchError(this.handleError)
        );
  }
}

import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { Subject, takeUntil } from 'rxjs';
import { SrvService } from '../srv.service';

interface Imagens {
  src: string;
  originalWidth: number;
  originalHeight: number;
  screenWidth: number;
  screenHeight: number;
  resizedWidth: number;
}
@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  providers: [
    { provide: CarouselConfig, useValue: { interval: 1500, noPause: false, showIndicators: true } }
 ],
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  public urlPath = 'http://localhost:4200/assets/images/';
  public fullpath = '/volumes/ssd/desenvolvimento/repos-git-sources/site-salao/txsalao-e/tx/src/assets/images/';
  public path = this.fullpath.replace(this.fullpath, '/assets/images');

  noWrapSlides = false;

  showIndicator = true;

  public slides: Array<Imagens> = [];

  private readonly destroy = new Subject<boolean>();
  
  constructor(private srv: SrvService) { }

  ngOnInit(): void {
    this.getImages('listaimagens');
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public loadData({ retorno }: { retorno: any; }): void {
    this.slides = retorno.data;
    console.log('slides', this.slides);
  }

  public getImages(path: string) {
    this.srv.getImages(path, this.fullpath)
      .pipe(
        takeUntil(this.destroy),
      )
      .subscribe((response: string) => {
        this.loadData({ retorno: response });
      }, (error) => {
        console.error(error);
      });
  }






  /*
  private loadDialog(indice: number,
                     src: string, 
                     originalWidth: number, 
                     originalHeight: number, 
                     screenWidth: number,
                     screenHeight: number,
                     resizedWidth: number): void {
   
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      hasBackdrop: true,
      indice: indice,
      src: src,
      originalWidth: originalWidth,
      originalHeight: originalHeight,
      screenWidth: Number(screenWidth),
      screenHeight: Number(screenHeight),
      resizedWidth: resizedWidth,
      array: this.listaImagens
    }
    const dialogRef = this.dialog.open(DetailImageGalleryComponent, dialogConfig);
  }
*/
}
